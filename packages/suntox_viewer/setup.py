from setuptools import setup

setup(
    name='suntox_viewer',
    version='1.0.0',
    description='interface between suntox project and qonfluens platform',
    packages=["suntox_viewer"],
    install_requires=["numpy"],
)

