#!/bin/bash

MODULES=$(ls -d */)

for m in $MODULES; do
    cd $m
    if [ -f setup.py ]; then
        echo -e "\n\n\n\t==== installing $m... ===\n\n\n"
        pip install .
    fi
    cd ..
done
