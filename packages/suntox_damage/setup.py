from setuptools import setup

setup(
    name='suntox_damage',
    version='1.0.0',
    description='TKTD module for suntox project',
    packages=["suntox_damage"],
    install_requires=["numpy"],
)

