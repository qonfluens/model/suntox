from setuptools import setup

setup(
    name='suntox_spread',
    version='1.0.0',
    description='pollutant diffusion module for suntox project',
    packages=["suntox_spread"],
    install_requires=["numpy"],
)

