from setuptools import setup

setup(
    name='suntox_swimmer',
    version='1.0.0',
    description='swimmer agent for suntox project',
    packages=["suntox_swimmer"],
    install_requires=["numpy"],
)

