from setuptools import setup

setup(
    name='suntox_species',
    version='1.0.0',
    description='natural species agents for suntox project',
    packages=["suntox_species"],
    install_requires=["numpy"],
)

